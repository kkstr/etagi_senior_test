<?php
class ResponseJSON
{
	public $Result = 0;	
	public $Errors = [];
	public $Data = [];
	
	
	public function __construct( $defResult = 0)
	{
		$this->Result = ($defResult) ? 1 : 0;
	}
	
	
	public function setResult( $data)
	{
		$this->Result = ($data) ? 1 : 0;
	}
	
	public function setData( $name, $data)
	{
		$this->Data[ $name] = $data;
	}
}