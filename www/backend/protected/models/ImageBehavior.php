<?php
class ImageBehavior extends CActiveRecordBehavior
{
	public $imagePath = '';
	public $imageField = '';

	public function getImageUrl(){
		return $this->getBaseImagePath() . $this->owner->{$this->imageField};
	}

	public function getImageThumbUrl(){
		return $this->getBaseImagePath() . 'small_' . $this->owner->{$this->imageField};
	}

	private function getBaseImagePath(){
		return Yii::app()->request->baseUrl . '/' . $this->imagePath . '/';
	}
	
	
	public function beforeSave($event){
		
		if ($file = CUploadedFile::getInstance($this->owner, $this->imageField)){
			
			//delete old
			if( !empty( $this->owner->{$this->imageField} )) 
				$this->deleteFile();
			
			//unique
			$filename = $this->getUniqueFileName(). '.' . strtolower($file->extensionName);
			
			$file->saveAs($this->imagePath . '/' . $filename );
			$this->owner->{$this->imageField} = $filename;
			
		}
	}
	
	public function beforeDelete($event){
		$this->deleteFile();
	}
	
	public function deleteFile(){
		if( file_exists($this->imagePath . '/' . $this->owner->{$this->imageField}))
			unlink($this->imagePath . '/' . $this->owner->{$this->imageField});
		$this->owner->{$this->imageField} = '';
	}
	
	
	protected function getUniqueFileName()
	{
		return md5( get_class(  $this->owner ) . time() );
	}
	
}