<?php

/**
 * This is the model class for table "{{shows}}".
 *
 * The followings are the available columns in table '{{shows}}':
 * @property integer $id
 * @property string $show_time
 * @property integer $FK_banner
 * @property integer $FK_pageurl
 * @property integer $FK_session
 */
class Show extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{shows}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('FK_banner, FK_pageurl, FK_session', 'required'),
			array('FK_banner, FK_pageurl, FK_session', 'numerical', 'integerOnly'=>true),
			array('show_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, show_time, FK_banner, FK_pageurl, FK_session', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'show_time' => 'Show Time',
			'FK_banner' => 'Fk Banner',
			'FK_pageurl' => 'Fk Pageurl',
			'FK_session' => 'Fk Session',
		);
	}

	public function logShowing( $banner, $sessionId, $currentUrl)
	{
		$model = new Show();
		$model->setAttributes([
			'show_time'	=> date( 'Y-m-d H:i:s'),
			'FK_banner' => $banner->id,
			'FK_pageurl' => $currentUrl,
			'FK_session' => $sessionId
		]);
		return $model->save();
		
	}
	
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('show_time',$this->show_time,true);
		$criteria->compare('FK_banner',$this->FK_banner);
		$criteria->compare('FK_pageurl',$this->FK_pageurl);
		$criteria->compare('FK_session',$this->FK_session);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Show the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
