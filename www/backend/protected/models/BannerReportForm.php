<?php
class BannerReportForm extends CFormModel
{
	public $dateFrom;
	public $dateTill;
	
	protected $statCache = [];	
	protected $statPCache = [];
	protected $ready = 0;
	
	
	public function rules()
	{
		return array(
				['dateFrom, dateTill', 'safe'],
	
		);
	}
	public function attributeLabels()
	{
		return array(
	
		);
	}
	
	public function getBannerList()
	{
		return Banner::model()->findAll();
	}
	
	public function pathDictionary()
	{
		$models = SitePath::model()->findAll();
		return CHtml::listData( $models, 'id', 'path');
	}
	
	public function getPStat($bid)
	{
		if( isset($this->statPCache[$bid]))
		{
			return $this->statPCache[$bid];
		}
		
		return [];
	}
	
	public function getStatValue( $bid, $code)
	{
		if( isset($this->statCache[$code][$bid]))
		{
			return $this->statCache[$code][$bid];
		}
	
		return 0;
	}
	
	public static function formatDateOut( $input )
	{
		// 'Y-m-d' to 'd.m.Y'
		$a = explode( '-', $input);
		if(count($a) == 3)
			return $a[2] . '.' . $a[1] . '.' . $a[0];
		else
			return '00.00.0000';
	}
	
	public function setDefaults()
	{
		$this->dateFrom = date("Y-m-d");
		$this->dateTill = date("Y-m-d");
	}
	
	
	protected function prepareParams()
	{
		$this->dateFrom = date("Y-m-d", strtotime($this->dateFrom));
		$this->dateTill = date("Y-m-d", strtotime($this->dateTill));
		
		
	}
	
	protected function getStat()
	{
		$DS = Yii::app()->db->createCommand();
		$DS->from( '{{shows}} s')
			->where('s.show_time >= :timeFrom')
			->andWhere( 's.show_time <= :timeTill');
		
		//a = количество показов за определенный период времени
		$DS_a = clone $DS;
		$DS_a->select( 'count(s.id) as s, s.FK_banner')->group('s.FK_banner');
		
		$params = [
			'timeFrom' => $this->dateFrom . " 0:0:0",
			'timeTill' => $this->dateTill . " 23:59:59",
		];
		$result = [];
		$stat = [];
		$rows = $DS_a->queryAll(1, $params);
		foreach ($rows as $line)
			$stat[ $line['FK_banner'] ] = $line['s'];
		
		$result['a'] = $stat;
		
		//c = количество уникальных просмотров баннера
		$DS_c = clone $DS;
		$DS_c->selectDistinct( 'count( distinct(s.FK_session)) as s, s.FK_banner')->group('s.FK_banner');
		
		$stat = [];
		$rows = $DS_c->queryAll(1, $params);
		
		foreach ($rows as $line)
			$stat[ $line['FK_banner'] ] = $line['s'];
		
		$result['c'] = $stat;
		
		// d = среднее количество просмотров баннера посетителем сайта
		// d = a/c 
		
		$this->statCache = $result;
		
		
		// статистика запросов баннера со страниц сайта за период времени
		$pageStat = [];
		$DS->select('count(s.id) as s, s.FK_banner, s.FK_pageurl')->group('s.FK_banner');
		$rows = $DS->queryAll(1, $params);
		
		foreach ($rows as $line)
		{
			if(!isset($pageStat[$line['FK_banner']]))
				$pageStat[$line['FK_banner']] = [];
			$pageStat[$line['FK_banner']][$line['FK_pageurl']]  = $line['s'];
		}
		$this->statPCache = $pageStat;
	}
	
	
	public function perform()
	{
		$this->prepareParams();
		
		$this->getStat();
		
		$this->ready = 1;
	}
	
	public function isReady()
	{
		return $this->ready;
	}
}