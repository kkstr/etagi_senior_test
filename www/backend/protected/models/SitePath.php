<?php

/**
 * This is the model class for table "{{pageurl}}".
 *
 * The followings are the available columns in table '{{pageurl}}':
 * @property integer $id
 * @property string $path
 */
class SitePath extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pageurl}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path', 'required'),
			array('path', 'length', 'max'=>250),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, path', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bannerLinks' => [self::HAS_MANY, 'BannerPaths', 'FK_path']
		);
	}
	


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'path' => 'Path',
		);
	}

	public function findUnlinked( $bannerId, $txt )
	{
		$criteria = new CDbCriteria();
		$criteria->join = 'LEFT JOIN {{bannerPaths}} bp ON bp.FK_path = t.id AND bp.FK_banner = :bid';
		$criteria->addCondition( 'isnull(bp.FK_banner)');
		$criteria->addCondition( 't.path LIKE (:pathlike)');
		$criteria->params = [
				'bid' => $bannerId,
				'pathlike' => $txt . "%"
		];
		$criteria->limit = 10;
		return $this->findAll( $criteria);
	}
	
	
	public function setDefaults( $referrer='/')
	{
		$this->path = $referrer;
	}
	
	public function getCurrentPath(  $request )
	{
		$pathData = parse_url($request->urlReferrer);
		$path = $pathData['path'];
		
		$model = $this->findByAttributes( ['path' => $path	]);

		if( $model === null)
		{
			$model = new SitePath();
			$model->setDefaults( $path );
			$model->save();
		}
		
		return $model->id;
	}
	
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('path',$this->path,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SitePath the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
