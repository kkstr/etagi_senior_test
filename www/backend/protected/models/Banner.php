<?php

/**
 * This is the model class for table "{{banners}}".
 *
 * The followings are the available columns in table '{{banners}}':
 * @property integer $id
 * @property string $name
 * @property string $path
 * @property integer $active
 * @property string $comment
 */
class Banner extends CActiveRecord
{
	public $imagePath = 'images'; 
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{banners}}';
	}

	public function setDefaults()
	{
		$this->active = 1;
		
	}
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('path', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>true, 'maxSize' => 524288, 'safe'=>false),
				
			array('name', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('comment', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, path, active, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'path_link' => [ SELF::HAS_MANY, 'BannerPaths', 'FK_banner'],
			'paths'	=> [self::HAS_MANY, 'SitePath', array('FK_path'=>'id'),'through'=>'path_link'],
			'countPaths'	=>	[self::STAT, 'BannerPaths', 'FK_banner' ]
		);
	}

	public function getFullPath()
	{
		if(!empty( $this->path))
			return Yii::getPathOfAlias('webroot') . '/'.  $this->imagePath . '/' . $this->path;
	}
	public function getSitePath()
	{
		if(!empty( $this->path))
			return  '/backend/'.  $this->imagePath . '/' . $this->path;
	}
	
	public function withPath( $pathId )
	{
		$this->getDbCriteria()->mergeWith([
				'join' => 'INNER JOIN {{bannerPaths}} bp ON bp.FK_banner = t.id',
				'condition' => 'bp.FK_path = :pid',
				'params'=>	[':pid'	=> 	$pathId ],
				'order' => 'RAND()'
		]);
		return $this;
	}
	
	public function loadForShow($currentPath)
	{
		return $this->withPath( $currentPath)->find();
	}
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'path' => 'Path',
			'active' => 'Active',
			'comment' => 'Comment',
		);
	}

	public function behaviors(){
		return array(
				'imageBehavior' => array(
						'class' => 'ImageBehavior',
						'imagePath' => $this->imagePath,
						'imageField' => 'path',
				),
		);
	}
	
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('comment',$this->comment,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Banner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
