<?php

class ActionUpdate extends CAction {
	
	public $view;
	public $layout;
	public $modelClass;
	public $backUrl;
	
	public function run() {
		
		$this->getController()->layout = $this->layout;
		$model = $this->loadModel(Yii::app()->request->getParam( 'id', false));
		
		$inputData = Yii::app()->request->getParam( $this->modelClass, false );
				
		if ( $inputData ) {
			
			$model->setAttributes($inputData);
		
			if ($model->validate() && $model->save()) {
				
				$this->getController()->redirect( $this->backUrl);
				
				Yii::app()->end();
			}
		}
		
		$this->getController()->render( $this->view, array(
			'model' => $model,	
		));
	
	}
	
	
	private function loadModel($id)
	{
		if($id===false)
			throw new CHttpException(404,'The requested page does not exist.');
		
		$className = $this->modelClass;
		$model = $className::model()->findByPk($id);
		
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		
		
		return $model;
	}
}