<?php

class ActionAjaxSearch extends CAction {
	
	public $modelClass;
	public $searchFunction;
	public $attributes = [ 'name', 'ID' ];
	
	public function run() {

		
		
		if(Yii::app()->request->isAjaxRequest && $txt = Yii::app()->request->getParam('q'))
		{
			$txt = trim( $txt);
				
			$className = $this->modelClass;
			$modelArray = $className::model()->{$this->searchFunction}( $txt );
			$returnVal = '';
				
			foreach($modelArray as $model)
			{
				$pack = [];
				foreach ($this->attributes as $attr)
					$pack[] = $model->getAttribute( $attr);
				
				$returnVal .= implode( '|', $pack ) . "\n";
			}
		
			echo $returnVal;
		}
	
	}
}