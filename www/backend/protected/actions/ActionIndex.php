<?php

class ActionIndex extends CAction {
	
	public $view;
	public $layout = 1;
	public $modelClass;
	
		
	public function run() {
			
// 		$filterManager = Yii::app()->filterDispatcher;
// 		$filter = $filterManager->getFilterModel( $this->modelClass);
		$class = $this->modelClass;
		$filter = new $class();
		$filter->unsetAttributes();
		
		if( $this->layout !== false )
		{
			$this->getController()->render( $this->view, [
				'filter'	=> $filter,
			]);
		}
		else 
		{
			$this->getController()->renderPartial( $this->view, [
					'filter'	=> $filter,
			]);
		}
	}
}