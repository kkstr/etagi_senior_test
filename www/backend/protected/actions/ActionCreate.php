<?php

class ActionCreate extends CAction {
	
	public $view;
	public $layout;
	public $modelClass;
	public $backUrl;
	
	public function run() {
		
		$this->getController()->layout = $this->layout;
		
		$model = new $this->modelClass;
		$model->setDefaults();
		
		$inputData = Yii::app()->request->getParam( $this->modelClass, false );
				
		if ( $inputData ) {
			
			$model->setAttributes($inputData);
		
			if ($model->validate() && $model->save()) {
				
				$this->getController()->redirect( $this->backUrl);
				
				Yii::app()->end();
				
			}
		
		}
		
		$this->getController()->render( $this->view, array(
			'model' => $model,	
		));
	
	}
}