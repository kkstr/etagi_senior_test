<?php

class m170527_120000_database extends CDbMigration
{
    
    private $t1 = '{{sessions}}';
    private $t2 = '{{shows}}';
    private $t3 = '{{banners}}';
    private $t4 = '{{pageurl}}';

    
    
  
    public function safeUp()
    {
        $this->createTable($this->t3, array(
        	'id'         => 'pk',
        	'name' => 'string NOT NULL',
        	'path' => 'string NOT NULL',
        	'active'	=>	'int(1) NOT NULL default 1',
        	'comment' => 'varchar(500)',
            		
        ), "DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
        
        $this->createTable($this->t1, array(
        		'id'		=>	'pk',
        		'FK_session'	=>	'varchar(250) not null',
        		'start_time'	=>	'datetime',
        		
        ), "DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
        
        $this->createTable($this->t4, array(
        		'id'		=>	'pk',
        		'path'		=>	'varchar(250) not null',      		
        ),  "DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
        
        $this->createTable($this->t2, array(
        		'id'         => 'pk',
        		'show_time'	=>	'datetime',
        		
        		'FK_banner'		=>	'int not null',
        		'FK_pageurl'	=>	'int not null',
        		'FK_session'	=>	'int not null',
        
        ),  "DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
        
        

    }

    public function safeDown()
    {
        $this->dropTable($this->t1);
        $this->dropTable($this->t2);
        $this->dropTable($this->t3);
        $this->dropTable($this->t4);
    
        return true;
    }
}