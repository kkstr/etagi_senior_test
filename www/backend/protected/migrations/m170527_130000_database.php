<?php

class m170527_130000_database extends CDbMigration
{
    
    private $t1 = '{{bannerPaths}}';
   

    
    
  
    public function safeUp()
    {
        $this->createTable($this->t1, array(
        	
        	'FK_banner' => 'INT NOT NULL',
        	'FK_path' => 'INT NOT NULL',
        	'PRIMARY KEY (`FK_banner`,`FK_path`)'
        		
        ), "DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
        
       
    }

    public function safeDown()
    {
     	$this->dropTable($this->t1);
     
        return true;
    }
}