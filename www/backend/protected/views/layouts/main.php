<?php /* @var $this Controller */ ?>
<?php

$themePath = Yii::app()->request->baseUrl;

Yii::app()->clientScript
    ->registerCssFile($themePath.'/assets/css/bootstrap.css')
    ->registerCssFile($themePath.'/assets/css/bootstrap-theme.css')
    ->registerCoreScript('jquery',CClientScript::POS_END)
    ->registerCoreScript('jquery.ui',CClientScript::POS_END)
    ->registerCoreScript('cookie',CClientScript::POS_END)
    ->registerScriptFile($themePath.'/assets/js/bootstrap.min.js',CClientScript::POS_END);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />



	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page" style="margin-top: 80px;">

	<div id="header">
	
	</div><!-- header -->

	<div id="mainmenu">
	<?php 
		$this->widget('bootstrap.widgets.BsNavbar', array(
			'brandLabel'	=>	false,
    		'collapse' => true,
			'htmlOptions' => array(
				'containerOptions' => 1
			),
		
			'position' => BsHtml::NAVBAR_POSITION_FIXED_TOP,
			'items'=>array(
				array(
					'class' => 'bootstrap.widgets.BsNav',
					'type' => 'navbar',
					'activateParents' => true,
					'items' => array( 
						array('label'=>'Баннеры', 'url'=>array('/banner/index'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'Страницы', 'url'=>array('/sitePath/index'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'Отчет', 'url'=>array('/banner/report'), 'visible'=>!Yii::app()->user->isGuest),
						array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
						array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			))),   
    ));
?>
		
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
