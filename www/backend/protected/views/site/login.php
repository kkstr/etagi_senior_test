<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
?>
<h2>Login</h2> 
<div class="row">
<div class="form col-sm-4 col-sm-offset-4">
<p>Please fill out the following form with your login credentials:</p>
<?php $form=$this->beginWidget('BsActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	
		<?php echo $form->textFieldControlGroup($model,'username'); ?>
		<?php echo $form->passwordFieldControlGroup($model,'password'); ?>
		
		<p class="hint">
			Hint: You may login with <kbd>admin</kbd>/<kbd>admin</kbd>.
		</p>
	

	<div class="rememberMe">
		<?php echo $form->checkBoxControlGroup($model,'rememberMe'); ?>
	
	</div>

	<div class="buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
