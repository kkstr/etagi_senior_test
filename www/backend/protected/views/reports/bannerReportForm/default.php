<?php
$this->render( '//reports/bannerReportForm/_form', [
			'model' => $model	
		]);
?>

<?php if( $model->isReady() ):?>

<legend>Простая статистика</legend>
<table class="table table-bordered">
<thead>
	<tr>
		<th>Баннер</th>
		<th class="col-sm-2">Всего показов</th>
		<th class="col-sm-2">Уникальных показов</th>
		<th class="col-sm-2">В среднем на сессию</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($model->getBannerList() as $bmodel):?>
	<tr>
		<td><?php echo '#'.$bmodel->id . ' ' . $bmodel->name?></td>
		<td class="numField"><?php echo $a = $model->getStatValue( $bmodel->id, 'a')?></td>
		<td class="numField"><?php echo $c = $model->getStatValue( $bmodel->id, 'c')?></td>
		<td class="numField"><?php 
		
			if( $c == 0 )
				$d = 0;
			else 
				$d = $a / $c;
			echo number_format($d, 3);
		?></td>
		</tr>
	<?php endforeach;?>
</tbody>
</table>

<?php 
	$paths = $model->pathDictionary();
?>
<legend>Запросы постранично</legend>
<table class="table table-bordered">
<thead>
	<tr>
		
		<th>Страница</th>
		
		<th class="col-sm-2">Всего показов</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($model->getBannerList() as $bmodel):?>
	<?php $bannerPStat = $model->getPStat( $bmodel->id )?>
	<tr>
		<td style="background-color: #EEE" colspan="2"><h5><?php echo '#'.$bmodel->id . ' ' . $bmodel->name?></h5></td>
	</tr>
	
		<?php foreach ($bannerPStat as $pathId=>$statRec):?>
		<tr>
			<td><?php echo $paths[ $pathId ]?></td>
			<td class="numField"><?php echo $statRec?></td>
			
		</tr>
		<?php endforeach;?>
	<?php endforeach;?>
</tbody>
</table>

<?php endif ?>


<style>
.numField{
	text-align: right;
}
</style>