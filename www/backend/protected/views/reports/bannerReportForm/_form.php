<?php


$this->beginWidget('bootstrap.widgets.BsPanel', array(
    'title' => 'Параметры отчета:',
    'type' => BsHtml::PANEL_TYPE_PRIMARY
));
?>

<?php $form=$this->beginWidget('BsActiveForm', array(
	'enableAjaxValidation'=>false,
)); ?>

<div class="row">	
	
	<?php echo $form->errorSummary($model); ?>	
	
	<div class=col-md-3>
		
		<?= BsHtml::label("Дата C", 'dateFrom')?>
		<div class="form-group">
			<?php $f = 'dateFrom';?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',  array(
						'name'=>CHtml::activeName($model, $f ),
						'language' => 'ru', 
						'value'=>(!empty($model->$f)) ? BannerReportForm::formatDateOut( $model->$f) : "", 
	  					'options' => array(
							'changeMonth' => 'true',
	 						'changeYear' => 'true',
							'showButtonPanel' => 'true', 
							'constrainInput' => 'false'),
						'htmlOptions' => array( 
							'class' => 'sender form-control',
							'placeholder'	=>	$model->getAttributeLabel($f),
							'title'	=>	$model->getAttributeLabel($f)
						)
			)); ?>
			
		</div>
	</div>
	<div class=col-md-3>
	<?= BsHtml::label("Дата По", 'dateTill')?>
		<div class="form-group">
			<?php $f = 'dateTill';?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker',  array(
						'name'=>CHtml::activeName($model, $f ),
						'language' => 'ru', 
						'value'=>(!empty($model->$f)) ? BannerReportForm::formatDateOut( $model->$f) : "",
	  					'options' => array(
							'changeMonth' => 'true',
	 						'changeYear' => 'true',
							'showButtonPanel' => 'true', 
							'constrainInput' => 'false'),
						'htmlOptions' => array( 
							'class' => 'sender form-control',
							'placeholder'	=>	$model->getAttributeLabel($f),
							'title'	=>	$model->getAttributeLabel($f)
							
						)
			)); ?>
			
		</div>
	
	</div>
	
	
	
	
	
</div>
	<div class="form-group">
		<?php echo BsHtml::submitButton('Сформировать отчет', array ( 'color' => BsHtml::BUTTON_COLOR_PRIMARY) ); ?>
	</div>

<?php 

$this->endWidget();
$this->endWidget(); 