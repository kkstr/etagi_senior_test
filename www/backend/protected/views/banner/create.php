<?php
/* @var $this BannerController */
/* @var $model Banner */

$this->breadcrumbs=array(
	'Banners'=>array('index'),
	'Create',
);

?>

<h1>Новый баннер</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>