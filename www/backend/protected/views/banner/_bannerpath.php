<div class="row subform" pathid="<?php echo $model->id ?>">
	<div class="col-sm-8">
		<?php echo BsHtml::emphasis( $model->path)?>			
	</div>
	<div class="col-sm-4">
		<?php  echo BsHtml::button( 'Удалить', [
			'class' => 'row_delete',
			'color' => BsHtml::BUTTON_COLOR_DANGER	
		])?>
	</div>
</div>
<?php
