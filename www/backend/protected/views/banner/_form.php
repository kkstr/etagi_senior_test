<?php
/* @var $this BannerController */
/* @var $model Banner */
/* @var $form BsActiveForm */
 
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    //        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => false,
    'id' => 'banner_form',
	'htmlOptions'=>['enctype'=>'multipart/form-data']
));
?>
<fieldset>
    <legend>Баннер</legend>
    
    
<?php
echo BsHtml::errorSummary($model);

echo $form->textFieldControlGroup($model, 'name');
echo $form->checkBoxControlGroup($model, 'active');
echo $form->textAreaControlGroup($model, 'comment');
echo $form->fileFieldControlGroup($model, 'path');

echo BsHtml::submitButton('Submit', array(
    'color' => BsHtml::BUTTON_COLOR_PRIMARY
));
?>
</fieldset>
<?php
$this->endWidget();
?>

    

