<?php
/* @var $this BannerController */
/* @var $model Banner */

$this->breadcrumbs=array(
	'Banners'=>array('index'),
	'#' . $model->id . ' - ManageLinks',
);


?>

<h1>Banner #<?php echo $model->id; ?> - "<?php echo $model->name; ?>" <small>Привязка страниц</small></h1>

<legend>Страницы, где используется баннер</legend>

<div class="path_list">
<?php 
	foreach ($model->paths as $path)
		$this->renderPartial( '_bannerpath', [
			'model' => $path
	])
?>
</div>

<legend>Добавить страницу</legend>
<div class="row add_link_form">
	<div class="col-sm-8">
			<div class="form-group">
			<?php
			$fld = 'FK_path';
			echo BsHtml::label("Страница", '');
			echo BsHtml::hiddenField('FK_path', '', array('fid'=>$fld));
			$this->widget('CAutoComplete',
					array(
							'name'=>'extra_1',
							'url'=>array('banner/ajaxUnlinkedPaths', 'banner' => $model->id),
							'max'=>10, //specifies the max number of items to display
							'minChars'=>1,
							'delay'=>500, //number of milliseconds before lookup occurs
							'matchCase'=>false, //match case when performing a lookup?
							'htmlOptions'=>array('size'=>'40', 'class' => 'form-control'),
							'methodChain'=>".result(function(event,item){
									\$(\"#FK_path\").val(item[1]);
								})",
							'value'=>'',
					));
			?>
			</div>				
	</div>
	<div class="col-sm-4">
		<?php  echo BsHtml::button( 'Добавить', [
			'class' => 'row_add',
			'color' => BsHtml::BUTTON_COLOR_PRIMARY	
		])?>
	</div>
</div>



<script type="text/javascript">
var BannerId = <?php echo $model->id?>;

$( ".row_add").click ( function() {
	
	var newPath = $( 'input#FK_path' ).val();
	if( newPath > 0 )
	{
		$.getJSON( "<?php echo $this->createUrl("banner/createLink")?>", { banner: BannerId, path: newPath} )
		 .done( function(data){
			 if( data.Result = 1)
			 {
				 addNewRow( data.Data);
				 $( 'input#FK_path' ).val(0);
			 }
		
		 });
		  
	}
})

function addNewRow( data)
{
	$( '.path_list').append( data.html);
	onSubform( $( '.subform:last'));
}









		
function onSubform( subform ) {
	$( subform ).find( ".row_delete").click ( function() {
		var delPath = $(subform).attr('pathid');
		if( delPath > 0)
		{
			$.getJSON( "<?php echo $this->createUrl("banner/deleteLink")?>", { banner: BannerId, path: delPath} )
			 .done( function(data){
				 if( data.Result = 1)
				 {
					 $(subform).remove();
				 }
			
			 });
			
		}
		
		
	});
}


















		
function updateLegalsList( legals )
{
	var listObj = $(  "#<?php echo CHtml::activeId($model, 'FK_legal_person' ) ?>");

	$( listObj ).empty();
	
	for( i=0; i<legals.length; i++ )
	{
		$( listObj ).append( $('<option value="'+ legals[i].ID+'">'+legals[i].name+'</option>'));
	}
	$( listObj ).val( legals[0].ID );
}

function updateBillsList( bills )
{
	var list = '';
	for( i=0; i<bills.length; i++ )
	{
		list += '<optgroup label="'+ bills[i].ID+'">';
		for( j=0; j<bills[i].values.length; j++ )
		{
			list +=  '<option value="'+ bills[i].values[j].ID+'">'+bills[i].values[j].name+'</option>';
		}
		list += '<optgroup>';		
	}

	var listObjs = $( document ).find("select[fid=FK_bill]");
	for( i=0; i<listObjs.length; i++ )
	{
		$( listObjs[i] ).empty();
		$( listObjs[i] ).append(list);
		$( listObjs[i] ).val( bills[0].ID );
	}
	

}

function updateValuesWithCA( clientId )
{

	//подгрузка
	 $.getJSON( "<?php echo Yii::app()->createUrl("payment/ajaxOnCAChange"); ?>?q="+clientId , function( data ) {

			updateLegalsList( data.legals );
			updateBillsList( data.bills);
	 	});
	
}

function enableElements(){

	
	for( var i=0; i<toggleFields.length; i++ )
	{
		var fid = toggleFields[i];
		$( 'input[fid='+fid+']').removeAttr('disabled');
		$( 'select[fid='+fid+']').removeAttr('disabled');
		$( 'textarea[fid='+fid+']').removeAttr('disabled');		
	}

	$( '#saveButton').css('display', 'inline-block');
	$( '.deleteLine').removeClass('disabled');
	$( '#newLineButton').css('display', 'inline-block');
}




$(document).on('ready', function(){	

	$( ".subform").each( function() {
		onSubform( this );
	});
	
	$( "a#toggleReadOnly").click ( function() {
		enableElements();
		$(this).hide();
	});

	$( "a#cancelButton").click ( function() {
		parent.$.fancybox.close();
	});

	$( "#newLineButton").click ( function() {
		var source = $( '.subform_template' ).html();
		source = source.replace(/#NEW#/g, lineNumber); 
		lineNumber++;
		$( '#documentLines' ).append( source );
		var linesAr = $( '#documentLines' ).children( ".subform" );
		var newLine = $( linesAr[ linesAr.length-1] );
		onSubform( newLine  ); 
		
		
	});

	$( "#cbx_accept_ready").change ( function() {
		$( '#saveButton').toggle();	
	});

	$( "#Payment_FK_payment_kind").change ( function() {
		fillPursesList( $(this).val() )	
	});

	
	<?php if( $model->isNewRecord ):  ?>	
	$( "#payment-form").submit ( function() {
		if ( $( "#cbx_accept_ready").is(':checked') ) return true;
		return false;
				
	});
	<?php endif ?>
});

</script>
