<?php
/* @var $this Controller */
/* @var $model Banner */

?>

<?php echo BsHtml::linkButton('Добавить', array(
	'url' => $this->createUrl('create'),
	
	'color' => BsHtml::BUTTON_COLOR_DEFAULT,
	'icon'=> BsHtml::GLYPHICON_PLUS,
	'style' => 'float: right;'
))?>

<h3>Баннеры</h3>


<?php



$grid = $this->widget('bootstrap.widgets.BsGridView',array(
		'type' => BsHtml::GRID_TYPE_STRIPED,
		'id'=>'main-grid',
		'dataProvider'=> $filter->search( ),
		'summaryText'=> "Выбрано <strong>{count}</strong> из " . Banner::model()->count( ),
		'columns'=>array(

			array(
					
				'name'	=>'id',
				'type'	=> 'raw',
				'value'	=>	'BsHtml::link($data->id, Yii::app()->createUrl( "banner/update", ["id"=>$data->id]))',
				
			),
			array(
					'name' => 'path',
					'type'	=>	'raw',
					'value' => function( $data){
						return BsHtml::imageThumbnail($data->getSitePath(), '', ['style' => 'max-height: 100px' ]);
				}
			),
			array(
						
				'name'	=>'name',
				'type'	=> 'raw',
				'value'	=>	'BsHtml::link($data->name, Yii::app()->createUrl( "banner/update", ["id"=>$data->id]))',
			
			),
			array(
			
				'header'	=>'Страниц подключено',
				'type'	=> 'raw',
				'value'	=>	'BsHtml::link($data->countPaths,  Yii::app()->createUrl( "banner/manageLinks", ["id"=>$data->id]))',
						
			),
			


		),
));
?>

