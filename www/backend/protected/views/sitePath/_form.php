<?php
/* @var $this SitePathController */
/* @var $model SitePath */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    //        'layout' => BsHtml::FORM_LAYOUT_HORIZONTAL,
    'enableAjaxValidation' => false,
    'id' => 'spath_form',
	
));
?>
<fieldset>
    <legend>Страница сайта</legend>
    
    
<?php
echo BsHtml::errorSummary($model);

echo $form->textFieldControlGroup($model, 'path');

echo BsHtml::submitButton('Submit', array(
    'color' => BsHtml::BUTTON_COLOR_PRIMARY
));
?>
</fieldset>
<?php
$this->endWidget();
?>