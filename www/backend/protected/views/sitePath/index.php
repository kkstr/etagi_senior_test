<?php
/* @var $this SitePathController */
/* @var $dataProvider CActiveDataProvider */

echo BsHtml::linkButton('Добавить', array(
	'url' => $this->createUrl('create'),
	
	'color' => BsHtml::BUTTON_COLOR_DEFAULT,
	'icon'=> BsHtml::GLYPHICON_PLUS,
	'style' => 'float: right;'
))?>

<h3>Страницы</h3>


<?php



$grid = $this->widget('bootstrap.widgets.BsGridView',array(
		'type' => BsHtml::GRID_TYPE_STRIPED,
		'id'=>'main-grid',
		'dataProvider'=> $filter->search( ),
		'summaryText'=> "Выбрано <strong>{count}</strong> из " . SitePath::model()->count( ),
		'columns'=>array(

			array(
					
				'name'	=>'id',
				'type'	=> 'raw',
				'value'	=>	'BsHtml::link($data->id, Yii::app()->createUrl( "sitePath/update", ["id"=>$data->id]))',
				
			),
			array(
						
				'name'	=>'path',
				'type'	=> 'raw',
				'value'	=>	'BsHtml::link($data->path, Yii::app()->createUrl( "sitePath	/update", ["id"=>$data->id]))',
			
			),
			
			


		),
));
?>
