<?php
/* @var $this SitePathController */
/* @var $model SitePath */

$this->breadcrumbs=array(
	'Site Paths'=>array('index'),
	'Update #' . $model->id,
);


?>

<h1>Update SitePath <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>