<?php
/* @var $this SitePathController */
/* @var $model SitePath */

$this->breadcrumbs=array(
	'Site Paths'=>array('index'),
	'Create',
);


?>

<h1>Новая страница</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>