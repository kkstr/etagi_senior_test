<?php

class BannerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
	
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','ajaxUnlinkedPaths','create','update', 'manageLinks', 'report', 'createLink', 'deleteLink'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	public function actionCreateLink( $banner, $path)
	{
		$model = new BannerPaths();
		$model->setAttributes( [
			'FK_banner' => $banner,
			'FK_path' => $path
		]);
		
		$response = new ResponseJSON(0);
		if(  $R = $model->save() )
		{
			$response->setResult( $R );
			$response->setData('html', $this->renderPartial( '_bannerpath', [
				'model' => $model->path
			], true));
		}
		
		$this->renderPartial( '//layouts/json', ['data' => $response]);
	}
	
	public function actionDeleteLink( $banner, $path)
	{
		$R = BannerPaths::model()->deleteAllByAttributes( [
				'FK_banner' => $banner,
				'FK_path' => $path
		]);
	
		$response = new ResponseJSON( ($R>0) ? 1 : 0 );
	
		$this->renderPartial( '//layouts/json', ['data' => $response]);
	}

	public function actions()
	{
		return array(
				 
				// сайты
				'index'=> [
						'class' 		=> 'application.actions.ActionIndex',
						'modelClass'	=> 'Banner',
						'view'			=> 'index',
				],
				 
				'create'=> [
						'class' 		=> 'application.actions.ActionCreate',
						'modelClass'	=> 'Banner',
						'view'			=> 'create',
				
				],
				 
				'update'=> [
						'class' 		=> 'application.actions.ActionUpdate',
						'modelClass'	=> 'Banner',
						'view'			=> 'update',
						'backUrl'		=>	$this->createUrl('index')
						
				],
				

			);
	}
	
	public function actionAjaxUnlinkedPaths($banner)
	{
		$banner = $this->loadModel($banner);
		
		if(Yii::app()->request->isAjaxRequest && $txt = Yii::app()->request->getParam('q'))
		{
			$txt = trim( $txt);
		
			$modelArray = SitePath::model()->findUnlinked( $banner->id, $txt );
			$returnVal = '';
		
			foreach($modelArray as $model)
			{
				$returnVal .= $model->path . "|" . $model->id ."\n";
			}
		
			echo $returnVal;
		}
	}
	
	public function actionView()
	{
		
		
		$sessionId = SiteSession::model()->getCurrentSession();
		$currentPath = SitePath::model()->getCurrentPath( Yii::app()->request );
			
		$model = Banner::model()->loadForShow($currentPath);

		if( $model === null)
			throw new CHttpException(404,'Нет подходящего баннера для показа на этой странице!');
		
		Show::model()->logShowing( $model, $sessionId, $currentPath);
		
		$this->renderPartial( '_view', [
			'model' => $model	
		]);
		
		
	}
	
	public function actionManageLinks($id)
	{
		$model = $this->loadModel($id);
	
		$filter = new SitePath('search');
		$filter->unsetAttributes();
	
		$this->render( 'manageLinks', [
				'model' => $model,
		]);
	
	
	}
	

	public function actionReport()
	{
		$report = new BannerReportForm;
		$report->setDefaults();
		
		if( $params = Yii::app()->request->getParam( 'BannerReportForm'))
		{
			$report->setAttributes( $params);
			$report->perform();
		}
		
		$this->render( '//reports/bannerReportForm/default', [
			'model' => $report	
		]);
	}
	
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Banner the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Banner::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Banner $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='banner-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
